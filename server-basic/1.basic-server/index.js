const http = require('http')
const fs = require('fs')

const server = http.createServer((req, res) => {
  fs.readFile('./index.html', (err, data) => {
    if (err) throw err
    res.end(data) // 버퍼를 보내지만 브라우저가 알아서 파싱을 한다.
  })
})

server.listen(3000, () => {
  console.log('server on 3000')
})

server.on('error', (err) => {
  console.error(err)
})
