const http = require('http')
const cookieParser = require('cookie-parser')
const server = http.createServer((req, res) => {
  console.log(req.headers.cookie)
  res.writeHead(200, { 'Set-Cookie': 'mycookie2=test2' })
  res.end('Hello Cookie')
})

server.listen(3000, () => {
  console.log('server on 3000')
})

server.on('error', (err) => {
  console.error(err)
})
