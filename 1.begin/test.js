const { a, b } = require('./simple')
const path = require('path')
console.log(path.dirname(__filename))
console.log(path.extname(__filename))
console.log(path.basename(__filename))
console.log(path.format(path.parse(__filename)))
console.log(path.normalize(__filename))
console.log(__filename)
console.log(path.join(__dirname, '..', '..', '/bet')) //절대경로 무시하고 함칩
console.log(path.resolve(__dirname, '..', '..', '/bet')) //절대경로 고려하고 합침

// console.log(a, b)
// console.log(__dirname)
// console.log(__filename)
// console.log(process)
