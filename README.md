# nodejs

노드js는 자바스크립트 런타임 환경
즉, 자바스크립트를 브라우저밖의 환경에서도 실행할수 있도록 해주는 프로그램. 

## REPL
ex: console, terminal

is a simple interactive computer programming environment that takes single user inputs, executes them, and returns the result to the user; a program written in a REPL environment is executed piecewise

Read, Evaluate, Print, Loop

## 호출스택과 이벤트루프, 태스크 큐
- 일반함수는 스택에 쌓이고 비동기함수 및 콜백(즉시실행함수제외)은 태스크 큐에 들어간다.
- 스택에 쌓인 함수는 스택에 쌓인순서 역순으로 실행하고 스택에서 나간다.
- 비동기함수가 쌓인 태스크큐에서는 이벤트 루프에 의해 호출스택으로 이동되고 호출 스택으로 이동되면 스택순에 따라 호출된다.
- 호출 스택이 모두 해소되면 끝!!
- 이벤트루프는 태스크큐의 순서를 알고 있다.

## 이벤트 기반, 싱글쓰레드, 논블러킹 IO

- 이벤트 기반:

 이벤트리스너 : 대기
 이벤트발생: 핸들러(콜백)을 통해 실행되는데 이 실행과정은 태스크큐 => 이벤트루프 => 호출스택의 흐름으로 진행된다. 이런 일련의 흐름이 이벤트를 기반으로 이루어지므로 이벤트 기반이라고 할 수 있다.

- 논블로킹 IO
실행순서가 눈의 보이는 흐름과 다름. 왜냐하면 콜백을 태스크 큐로 보내버리는 과정이 있기 때문.. 이렇게 실행순서가 눈의 보이는 흐름과 다를때 논블로킹이라고 생각하면 됨. 이 원인은 싱글쓰레드이기 때문에 더욱 논블로킹을 해야함.

논블로킹 IO에는 파일시스템과 네트워크를 크게 예를 들수있음. 이놈들은 동시에 여러 일들을 효율적으로 처리해야하므로. 

worker를 통해서 싱글쓰레드를 극복하려고 하고 있음.

## 노드 모듈 시스템
module.exports = {}
module.exports = default Module

## console
console.time
console.timeend
console.dir
console.trace
console.log
console.error
console.warn

## __filename, __dirname, process
## path, os
path는 중요하므로 한번더 체크바람

## nodejs 장점

- 노드js는 아무것도 없는 상태에서 시작해서 하나씩 모듈을 끼워가는 재미가 있음
- 데이터를 수정하고 저장하는등 실시간으로 처리하는 데이터 작업이 많은경우에 좋음.

## nodejs 단점

- 노드 js는 하드웨어의 대한 작업들을 할 수가 없음. 따라서 이미지나, 인코딩, 메모리, 램의 대한 작업들을 위한 다면 장고(파이썬)을 사용하는 것이 좋다.

## nodejs의 사용 예

백엔드는 여러 언어로 다뤄진다. 다만 그중 데이터를 처리하고 다루는 경우는 nodejs를 사용할 것이다.
paypal, uber, netflix
